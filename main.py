from fastapi import FastAPI,Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm, OAuth2AuthorizationCodeBearer
from models import User
from auth import get_user_info
import models
from pydantic import BaseModel, Field
from database import engine, SessionLocal
from sqlalchemy.orm import Session

app = FastAPI()

models.Base.metadata.create_all(bind=engine) #Erstellung der Datenbank

def get_db(): 
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

class Portfolio(BaseModel):
    kunde: str = Field(min_length=1)
    businessunit: str = Field(min_length=1)
    mitarbeiter: int = Field(gt=-1, lt=101)

PORTFOLIOS = []


@app.get("/")
def root(db: Session = Depends(get_db)):
    return "FastAPITest"


@app.get("/secure")
async def read_api(db: Session = Depends(get_db), user: User = Depends(get_user_info)):
    return db.query(models.Portfolio).all()

@app.post("/secure/create_kunde")
def create_kunde(portfolio: Portfolio, db: Session = Depends(get_db), user: User = Depends(get_user_info)):
    portfolio_model = models.Portfolio()
    portfolio_model.kunde = portfolio.kunde
    portfolio_model.businessunit = portfolio.businessunit
    portfolio_model.mitarbeiter = portfolio.mitarbeiter

    db.add(portfolio_model)
    db.commit()

    return portfolio

@app.delete("/secure/delete_portfolio/{portfolio_id}")
def delete_portfolio(portfolio_id: int, db: Session = Depends(get_db), user: User = Depends(get_user_info)):

    book_model = db.query(models.Portfolio).filter(models.Portfolio.id == portfolio_id).first()

    if book_model is None:
        raise HTTPException(
            status_code=404,
            detail=f"Eintrag mit der ID {portfolio_id} konnte nicht gefunden werden"
        )
    
        

    db.query(models.Portfolio).filter(models.Portfolio.id == portfolio_id).delete()

    db.commit()
    return "Eintrag wurde gelöscht"