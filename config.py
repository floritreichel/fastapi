from models import authConfiguration


settings = authConfiguration(
    server_url="http://127.0.0.1:8080/",
    realm="FastAPITest",
    client_id="FastAPI",
    client_secret="",
    authorization_url="http://127.0.0.1:8080/realms/FastAPITest/protocol/openid-connect/auth",
    token_url="http://127.0.0.1:8080/realms/FastAPITest/protocol/openid-connect/token",
)