from pydantic import BaseModel, EmailStr
from database import Base
from sqlalchemy import Column, String, Integer

class User(BaseModel):
    id: str
    username: str
    email: str
    first_name: str
    last_name: str
    realm_roles: list
    client_roles: list

class authConfiguration(BaseModel):
        server_url: str
        realm: str
        client_id: str
        client_secret: str
        authorization_url: str
        token_url: str

class Portfolio(Base) :
    __tablename__ = "portfolio"
    
    id = Column(Integer, primary_key=True, index=True)
    kunde = Column(String)
    businessunit = Column(String)
    mitarbeiter = Column(Integer)